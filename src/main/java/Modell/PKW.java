/**
 * 
 */
package Modell;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * @author Erkan Seng�l
 *
 */
public class PKW extends Fahrzeug {

	private Date Uberprufung;

	/**
	 * 
	 */
	public PKW() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param marke
	 * @param modell
	 * @param baujahr
	 * @param grundpreis
	 */
	public PKW(String marke, String modell, int baujahr, double grundpreis, Date uberprufungs) {
		super(marke, modell, baujahr, grundpreis);
		this.Uberprufung = uberprufungs;
	}

	public Date getUberprufung() {
		return Uberprufung;
	}

	public void setUberprufung(Date uberprufung) {
		Uberprufung = uberprufung;
	}

	
	public double getRabatt() {
		/**
		 * Aktuelle Jahr
		 */
		int Jahr = Calendar.getInstance().get(Calendar.YEAR);
		/**
		 * Uberjahr ubernimmt den Jahr der Uberprufung
		 */

		GregorianCalendar c = new GregorianCalendar();

		c.setTime(getUberprufung());
		int UberJahr = c.get(Calendar.YEAR);
		/**
		 * @return den Rabatt fuer PKW mit den vererbten Methode von "Fahrzeug"
		 */
		double v = getAlter() * 0.05 + (Jahr - UberJahr) * 0.02;

		if (v > 0.15) {
			v = 0.15;
		}

		return v;
	}
	
	public String toString() {
		SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
		DecimalFormat decimal = new DecimalFormat("#0.00");
		return    "\nTyp: PKW"
				+ "\nId: " + getId()
				+ "\nMarke: " + getMarke()
				+ "\nModell: " + getModell()
				+ "\nBaujahr: " + getBaujahr()
				+ "\nGrundpreis: " + decimal.format(getGrundpreis())
				+ "\nUeberprufungsdatum:" + s.format(getUberprufung());
				//+ "\nPreis: " + decimal.format(getPreis());
	}

}
