/**
 * 
 */
package Modell;

import java.io.Serializable;
import java.util.Calendar;

import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Erkan Seng�l
 *
 */
@ManagedBean
@Entity(name = "fahrzeug")
@Table(name = "fahrzeug")
public  class Fahrzeug implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "id")
	private int id;
	@Column(name = "Marke")
	private String Marke;
	@Column(name = "Modell")
	private String Modell;
	@Column(name = "Baujahr")
	private int Baujahr;
	@Column(name = "Grundpreis")
	private double Grundpreis;

	public Fahrzeug() {

	}

	public Fahrzeug(String marke, String modell, int baujahr, double grundpreis) {
		super();

		Marke = marke;
		Modell = modell;
		Baujahr = baujahr;
		Grundpreis = grundpreis;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMarke() {
		return Marke;
	}

	public void setMarke(String marke) {
		Marke = marke;
	}

	public String getModell() {
		return Modell;
	}

	public void setModell(String modell) {
		Modell = modell;
	}

	public int getBaujahr() {
		return Baujahr;
	}

	public void setBaujahr(int baujahr) {
		int jahr = Calendar.getInstance().get(Calendar.YEAR);
		if(baujahr>jahr) {
			throw new IllegalArgumentException("Baujahr liegt in der Zukunft");
		}
		Baujahr = baujahr;
	}

	public double getGrundpreis() {
		return Grundpreis;
	}

	public void setGrundpreis(double grundpreis) {
		Grundpreis = grundpreis;
	}
	
	public int getAlter(){
		int Jahr= Calendar.getInstance().get(Calendar.YEAR);
		 return Jahr-getBaujahr();
		
	}

	/*public  double getRabatt();

	public double getPreis() {
		return getGrundpreis() - (getGrundpreis() * getRabatt());
	}

*/
}
