/**
 * 
 */
package Modell;

import java.text.DecimalFormat;

/**
 * @author yakoc
 *
 */
public class LKW extends Fahrzeug {

	/**
	 * 
	 */
	public LKW() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param marke
	 * @param modell
	 * @param baujahr
	 * @param grundpreis
	 */
	public LKW(String marke, String modell, int baujahr, double grundpreis) {
		super(marke, modell, baujahr, grundpreis);
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see Modell.Fahrzeug#getRabatt()
	 */

	public double getRabatt() {
		
		double lkwrabatt = getAlter()*0.05;
		if (lkwrabatt > 0.2) {
			lkwrabatt = 0.2;
		}
		return lkwrabatt;
	}

	public String toString() {
		DecimalFormat decimal = new DecimalFormat("#0.00");
		return    "\nTyp: LKW"
				+ "\nId: " + getId()
				+ "\nMarke: " + getMarke()
				+ "\nModell: " + getModell()
				+ "\nBaujahr: " + getBaujahr()
				+ "\nGrundpreis: " + decimal.format(getGrundpreis());
				//+ "\nPreis: " + decimal.format(getPreis());
	}
}
