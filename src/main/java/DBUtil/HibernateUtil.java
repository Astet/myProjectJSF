package DBUtil;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
	//@SuppressWarnings("unchecked")
	public static List<Object> getAllObjects(Class clasz, String tableName) {
		
		List<Object> objects = new ArrayList<Object>();
		
		SessionFactory sessionFactory = new Configuration()
				.configure("hibernate.cfg.xml").addAnnotatedClass(clasz).buildSessionFactory();
		
		Session session = sessionFactory.getCurrentSession();

		try {

			session.beginTransaction();
			objects = session.createQuery("from "+tableName).getResultList();
			session.getTransaction().commit();
		} finally {
			session.close();
		}

		return objects;
	}
	
	
	public static void addObject(Object object) {
		
		SessionFactory sessionFactory = new Configuration()
				.configure("hibernate.cfg.xml").addAnnotatedClass(object.getClass()).buildSessionFactory();
		
		Session session = sessionFactory.getCurrentSession();
		try {
			session.beginTransaction();
			session.save(object);
			session.getTransaction().commit();
		} finally {
			session.close();
		}

	}
	
	
	public static void deleteObject(Class clasz,int Id) {
		
		SessionFactory sessionFactory = new Configuration()
				.configure("hibernate.cfg.xml").addAnnotatedClass(clasz).buildSessionFactory();
		
		Session session = sessionFactory.getCurrentSession();
		try {
			
			session.beginTransaction();
			Object newObject = session.get(clasz, Id);
			session.delete(newObject);
			session.getTransaction().commit();
		} finally {
			session.close();
		}

	}
	
	
	public static void updateObject(Class clasz,int Id) {
		
		SessionFactory sessionFactory = new Configuration()
				.configure("hibernate.cfg.xml").addAnnotatedClass(clasz).buildSessionFactory();
		
		Session session = sessionFactory.getCurrentSession();
		try {
			
			session.beginTransaction();
			Object newObject = session.get(clasz, Id);
			session.save(newObject);
			session.getTransaction().commit();
		} finally {
			session.close();
		}

	}


}
