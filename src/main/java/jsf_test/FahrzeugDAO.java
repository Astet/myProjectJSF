package jsf_test;

import java.util.List;

import Modell.Fahrzeug;

public interface FahrzeugDAO {

	public List<Fahrzeug> getFahrzeuglist();

	public void addFahrzeug();

	public void speichereFahrzeg();

	public void deleteFahrzeug(int id);

}
