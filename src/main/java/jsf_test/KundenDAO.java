package jsf_test;

import java.util.List;

import Modell.Kunde;

public interface KundenDAO {
	
	public void addKunde();

	public List<Kunde> getAllKunden();

	public void deleteKunde(int id);

	public void updateKunde(Kunde kunde);

}
