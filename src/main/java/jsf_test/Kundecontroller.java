package jsf_test;

import java.io.IOException;
import java.io.Serializable;
/*
 * @Author Erkan Seng�l
 * 
 * Erstellung eines Controller f�r Datenbank und Viewseite.
 */
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.application.ViewHandler;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import DBUtil.HibernateUtil;
import Modell.Kunde;

@ManagedBean(name="kundecontroller")
@javax.enterprise.context.SessionScoped
public class Kundecontroller implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//@ManagedProperty("#{kunde}")
	private Kunde kunde;

	private List<Kunde> allKunden;

	/**
	 * 
	 */

	

	public Kundecontroller() {

	}
	
	
	@PostConstruct
	public void init() {

		kunde = new Kunde();
		allKunden =  (List<Kunde>)(Object)HibernateUtil.getAllObjects(Kunde.class, "kunde");
		
	}

	public Kundecontroller(List<Kunde> Kunden) {
		
	}
	public void refresh() throws IOException {
		FacesContext.getCurrentInstance().getExternalContext().redirect("NewFile.xhtml");
	}



	public Kunde getKunde() {
		return kunde;
	}


	public void setKunde(Kunde kunde) {
		this.kunde = kunde;
	}


	public List<Kunde> getAllKunden() {
		return allKunden;
	}


	public void setAllKunden(List<Kunde> allKunden) {
		this.allKunden = allKunden;
	}


	public void addKunde() throws IOException {
		
		HibernateUtil.addObject(kunde);
		refresh();
	}

	public void deleteKunde(int id) throws IOException {
		
		HibernateUtil.deleteObject(Kunde.class, id);
		refresh();
	}

	public void updateKunde(int id) throws IOException {
		HibernateUtil.updateObject(Kunde.class, id);
		refresh();
	}

	
}
