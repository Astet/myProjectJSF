package jsf_test;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;

import DBUtil.HibernateUtil;
import Modell.Fahrzeug;

@ManagedBean
@SessionScoped
public class FahrzeugController implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Fahrzeug fahrzeug;
	
	private List<Fahrzeug> Fahrzeuge;

	public FahrzeugController() {
		
	}
	
	@PostConstruct
	public void init() {
		//fahrzeug = new Fahrzeug();
		Fahrzeuge = (List<Fahrzeug>)(Object)HibernateUtil.getAllObjects(Fahrzeug.class, "fahrzeug");
	}

	public Fahrzeug getFahrzeug() {
		return fahrzeug;
	}

	public void setFahrzeug(Fahrzeug fahrzeug) {
		this.fahrzeug = fahrzeug;
	}

	public List<Fahrzeug> getFahrzeuge() {
		return Fahrzeuge;
	}

	public void setFahrzeuge(List<Fahrzeug> fahrzeuge) {
		this.Fahrzeuge = fahrzeuge;
	}
	
	public void addFahrzeug() {
		HibernateUtil.addObject(fahrzeug);
	}
	
	public void deleteFahrzeug(int id) {
		HibernateUtil.deleteObject(Fahrzeug.class, id);
	}
	
	

}
