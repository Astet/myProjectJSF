$(document).ready(function(){
    //switch the tabs between login and register
    $('#login-form-link').click(function(e) {
		$("#login-form").delay(100).fadeIn(100);
 		$("#register-form").fadeOut(100);
		$('#register-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
    });
    $('#register-form-link').click(function(e) {
		$("#register-form").delay(100).fadeIn(100);
 		$("#login-form").fadeOut(100);
		$('#login-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
    });

    
    //login function
    $("#login-submit").click(function() {
        var email = $("#email").val();
        var password = $("#password").val();
        var json = {
            "email": email,
            "password": password
         };
       
       //default version without user from testuser.json
        window.location.href = "NewFile.xhtml";
    });
    
    
    $("#blockreg").hide();

    $("#register").click(function(){
        $("#blockreg").show();
    });


     



    });
